import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoServiceVerticle;

public class App {

    public static void main(String... args) {
        JsonObject jsonMongoDBOptions = new JsonObject();
        jsonMongoDBOptions.put("address", "mongodb-persistor");
        DeploymentOptions mongoDBOptions = new DeploymentOptions();
        mongoDBOptions.setConfig(jsonMongoDBOptions);

        Vertx.factory.vertx().deployVerticle(new MongoServiceVerticle(), mongoDBOptions);
    }
}
